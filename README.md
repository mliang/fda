# fdba

### 介绍
快速数据库访问（Fast DB Access），基于springboot的中间件，适用数据库MySql。
在项目中引入jar包，配置好数据库，即可以将数据库表映射成接口，节省开发初期的接口配置时间

### 架构
```
fdba.core
         .controller
                    .OutController.java           ------数据库表数据查询控制器
                    .TableController.java         ------数据库表结构及表字段查询控制器
         .entity  
                    .TableList.java               ------存储数据库表结构
         .scanning
                    .TableScanning.java           ------数据库表扫描器
                    .FieldScanning.java           ------数据库字段扫描器
         .service
                    .OutService.java              ------数据库表数据查询
         .Main.java                               ------启动加载扫描器

```
### 使用说明

#### 引用
1. 项目打包 mvn package
2. 找到jar包，target/fdba-0.0.1-SNAPSHOT.jar
3. 将jar包放加入本地Maven库
```
mvn install:install-file -Dfile=D:\fdba-0.0.1-SNAPSHOT.jar -DgroupId=com.ml.fdba -DartifactId=fdba -Dversion=0.0.1 -Dpackaging=jar
```
4. 配置pom.xml
```
<dependency>
      <groupId>com.ml.fdba</groupId>
      <artifactId>fdba</artifactId>
      <version>0.0.1</version>
</dependency>
```

####  配置application.properties

```
fdba.urlRoot = fdba
fdba.lib = ***

spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=jdbc:mysql://****:**/**?useSSL=false&useUnicode=true&characterEncoding=utf-8&autoReconnect=true&serverTimezone=Asia/Shanghai
spring.datasource.username=***
spring.datasource.password=***
```

####  接口访问

##### 查询数据表结构

1. 查询所有表及字段
- GET: /fdba/tables/all

2. 查询单表字段
- GET：/fdba/tables/{tableName}/fields

##### 查询表数据

1. 查询表全部数据
- GET：/fdba/out/{tableName}

2. 带条件查询表数据
- POST：/fdba/out/{tableName}
- body: {字段名1: 查询值1, 字段名2: 查询值2}

