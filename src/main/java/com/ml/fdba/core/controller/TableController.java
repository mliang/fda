package com.ml.fdba.core.controller;

import com.ml.fdba.core.entity.TableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/${fdba.urlRoot}/tables/")
public class TableController {

    @Autowired
    private TableList tableList;

    @GetMapping("/all")
    public List getAllTable(){
        return tableList.getTableList();
    }

    @GetMapping("/{table}/fields")
    public List getTableFields(@PathVariable("table")String tableName){
        return tableList.getTableFields(tableName);
    }

}
