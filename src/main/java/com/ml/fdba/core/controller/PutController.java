package com.ml.fdba.core.controller;

import com.ml.fdba.core.service.PutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/${fdba.urlRoot}/put/")
public class PutController {

    @Autowired
    public PutService putService;

    @PutMapping("/{table}")
    public String put(@PathVariable("table")String tableName, @RequestBody Map data){
        putService.update(tableName, data);
        return "success";
    }

    @PostMapping("/{table}")
    public String post(@PathVariable("table")String tableName, @RequestBody Map data){
        putService.save(tableName, data);
        return "success";
    }

    @DeleteMapping("/{table}/{id}")
    public void delete(@PathVariable("table")String table,@PathVariable("id")String id){

    }

}
