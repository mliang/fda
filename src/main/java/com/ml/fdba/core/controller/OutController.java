package com.ml.fdba.core.controller;

import com.ml.fdba.core.service.OutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/${fdba.urlRoot}/out/")
public class OutController {

    @Autowired
    private OutService outService;

    @GetMapping("/{table}")
    public List getList(@PathVariable("table")String tableName){
        return  outService.queryTableList(tableName, null);
    }

    @PostMapping("/{table}")
    public Object query(@PathVariable("table")String tableName,@RequestBody(required = false) Map condition){
        return outService.queryTableList(tableName, condition);
    }


}
