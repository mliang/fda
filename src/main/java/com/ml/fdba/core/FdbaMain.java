package com.ml.fdba.core;

import com.ml.fdba.core.scanning.FieldScanning;
import com.ml.fdba.core.scanning.TableScanning;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class FdbaMain implements ApplicationRunner {

    @Autowired
    TableScanning tableScanning;

    @Autowired
    FieldScanning fieldeScanning;

    @Override
    public void run(ApplicationArguments args) throws Exception{
        tableScanning.scanning();
        fieldeScanning.scanning();
    }

}
