package com.ml.fdba.core.scanning;

import com.ml.fdba.core.entity.TableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Component
public class FieldScanning {

    @Value("${fdba.lib}")
    private String lib;

    @Autowired
    private TableList tableList;

    @Resource
    private JdbcTemplate jdbcTemplate;

    public void scanning(){
        getAllField();
    }

    private void getAllField(){
        tableList.getTableList().forEach(table ->{
            String sql = "select column_name,column_comment,data_type  from information_schema.columns  where table_name= ? and table_schema = '"+lib +"'";
            List<Map<String, Object>> fieldList  = jdbcTemplate.queryForList(sql, new Object[]{table.getName()});
            table.setFieldList(fieldList);
        });
    }
}
