package com.ml.fdba.core.scanning;

import com.ml.fdba.core.entity.TableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Component
public class TableScanning {

    @Value("${fdba.lib}")
    private String lib;

    @Autowired
    private TableList tableList;

    @Resource
    private JdbcTemplate jdbcTemplate;

    public void scanning(){
        getAllTable();
    }

    private void getAllTable(){
        String sql = "show tables from ";
        List<Map<String, Object>> tableNameList = jdbcTemplate.queryForList(sql + lib);
        tableList.setTableList(tableNameList);
    }


}
